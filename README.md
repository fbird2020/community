# openLooKeng Community

Welcome to openLooKeng Community.


## Introduction

The Community repo is to store all the information about openLooKeng Community, inclouding governance, how to contribution, Communications and etc. 

## Folders

- content: In this folder you can find the introduction about governance of openLooKeng Community. The information is in English and Chinese. And it is synchronized to the menu Community in [openLooKeng website](openlookeng.io).

- design: in this folder you can find the design documents to explain what information is in what files.


## How to contribute

When openLooKeng community is updated, the information should be updated as well here. If you would like to help update the information in this repo, you are very appreciated. 

Please read [How to contribute](CONTRIBUTING.md) to get detailed guidance.

## Committers

1. Fred Li(@zerodefect), since June, 2020

1. Zeng Chen(@zengchen1024), since June, 2020

1. freesky-edward(@freesky-edward), since June, 2020


## Contact

To be added.

## Meeting

To be added.
